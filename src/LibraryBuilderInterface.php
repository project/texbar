<?php

namespace Drupal\texbar;

/**
 * Markitup library builder interface.
 */
interface LibraryBuilderInterface {

  const CDN_URL = 'https://raw.githubusercontent.com/pmagunia/markitup/refs/heads/master/src/markitup-1.x.jquery.js';

  const LIBRARY_PATH = '/libraries/markitup/';

  /**
   * Builds a definition for Markitup library.
   *
   * @return array
   *   Markitup library definition.
   */
  public function build();

}
